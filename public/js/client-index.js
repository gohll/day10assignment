
var RegApp = angular.module("RegApp", []);

(function(){

    var RegCtrl;

    RegCtrl = function($http){
        
        var ctrl = this;
        // initialize ng-model field on form
        ctrl.email = "";
        ctrl.password = "";
        ctrl.name = "";
        ctrl.gender = "";
        ctrl.birthDate = "";
        ctrl.address = "";
        ctrl.countryCode = "sg";
        ctrl.tel = "";
        ctrl.errormsg = "";
        // response message
        ctrl.status = {
            message: "",
            code: 0
        };

        // var bDate = document.getElementById('birthDate');
        // bDate.addEventListener('change', validate);
        //
        // ctrl.age = 0;
        // var validate = function(dDate) {           
        //     // console.log('event triggered');
        //     // check age > 18years
        //     // submit.classList.add('disabled');
        //     // submit.disabled = true;
        //     var date = new date();
        //     var age = Year(date-  dDate);
        //     if (age < 18) {
        //             ctrl.errormsg = "You must be at least 18 years old.!"
        //             submit.disabled = true;
        //         }
        // };

        ctrl.register = function() {
            $http.post("/register",{
                params : {
                        email : ctrl.email,
                    password  : ctrl.password,
                         name : ctrl.name,
                       gender : ctrl.gender,
                    birthDate : ctrl.birthDate,
                      address : ctrl.address,
                  countryCode : ctrl.countryCode,
                          tel : ctrl.tel
                }
            }).then(function(){
                console.info(" Success");
               
                ctrl.status.message ="Your registration is complete. Thank you.";
                ctrl.status.code = 202;
            }).catch(function(){
                console.info(" Error");
                ctrl.status.message = "Your registration failed.";
                ctrl.status.code = 400;
            });
        };
    };

    RegApp.controller("RegCtrl", ['$http', RegCtrl]);
})();

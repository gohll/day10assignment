//Load express
var express = require("express");

//Create an instance of express application
var app = express();

var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());

// Serves files from public directory, __dirname being absolute path
app.use(express.static(__dirname + "/public"));
app.use( express.static(__dirname + "/public/bower_components"));

// Use function below to handle unsuccessful posting to server
// app.use(function(req , res, next) {
//     res.redirect("error.html");
// });

app.post("/register", function(req, res){
        var email = req.body.params.email;
        // var password = req.body.params.password;
        var name = req.body.params.name;
        var gender = req.body.params.gender;
        var birthDate = req.body.params.birthDate;
        var address = req.body.params.address;
        var countryCode = req.body.params.countryCode;
        var tel = req.body.params.tel;
        console.info("Email : %s", email);
        console.info("Name: %s", name);
        console.info("Gender: %s", gender);
        console.info("Birth Date%s", birthDate) ;
        console.info("Address: %s", address);
        console.info("Country: %s", countryCode);
        console.info("Contact No: %s", tel) ;
        res.redirect("thankyou.html");
        res.status(200).end();
});

//
// app.get("/thankyou", function(req, res){
//         res.redirect("thankyou.html");
// });

//Start the web server on port 3000 unless specified
var portNumber = process.argv[2] || 3000;
console.info(process.argv[2]);

app.listen(parseInt(portNumber), function(){
    console.info("Webserver started on port %s", portNumber);
});
